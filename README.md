### Installation

First thing have to install expo in global then install all dependencies

follow this command

```bash
npm i expo -g
npm i
```

### Run app

Run this command

```bash
npm start
```

App will display the barcode on browser that use expo for run your device


### Generate apk file for android

Run this command

```bash
npm run build:android
```

