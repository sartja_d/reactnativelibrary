import firebase from 'firebase/app'
import 'firebase/auth'
import 'firebase/database'
import 'firebase/storage'
// import { Platform } from 'react-native'
import config from './config'

// if (Platform.OS !== 'web') {
//   window = undefined
// }

if (!firebase.apps.length) {
  firebase.initializeApp(config)
}
const auth = firebase.auth()
const db = firebase.database()
const storage = firebase.storage()
export {
  auth,
  db,
  storage,
}
