import { StyleSheet } from 'react-native'
import Colors from './Colors'

const Styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.grey,
  },
  contentContainer: {
    paddingTop: 30,
  },
  mx10: {
    marginHorizontal: 10,
  },
  my10: {
    marginVertical: 10,
  },
  my20: {
    marginVertical: 20,
  },
  mt20: {
    marginTop: 20,
  },
  mb10: {
    marginBottom: 10,
  },
  mb20: {
    marginBottom: 20,
  },
  mr10: {
    marginRight: 10,
  },
  mr20: {
    marginRight: 20,
  },
  ml20: {
    marginLeft: 20,
  },
  centerElement: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  bold: {
    fontWeight: 'bold'
  }
})

export default Styles


// module.exports = StyleSheet.create({

//   alwaysred: {
//       backgroundColor: 'red',
//       height: 100,
//       width: 100,
//   },

//   });
