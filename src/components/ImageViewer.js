import React from 'react'
import {
  Dimensions,
  Image,
  StyleSheet,
  Text,
  TouchableOpacity,
  View
} from 'react-native'

export default class ImageViewer extends React.Component {
  render() {
    const { onClose, uri } = this.props
    return (
      <View style={styleSelf.imageView}>
        <View
          style={{
            backgroundColor: '#333',
            padding: 20,
          }}>
          <TouchableOpacity onPress={onClose}>
            <Text style={{ color: '#fff' }}>ปิด</Text>
          </TouchableOpacity>
        </View>
        <Image
          style={{ width: Dimensions.get('window').width, height: Dimensions.get('window').height - 50 }}
          source={{ uri }}
        />
      </View>
    )
  }
}


const styleSelf = StyleSheet.create({
  imageView: {
    position: 'absolute',
    left: 0,
    top: 0,
    right: 0,
    bottom: 0,
    zIndex: 100,
  },
})
