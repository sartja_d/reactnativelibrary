import React from 'react'
import { Text } from 'react-native'

class ValidationText extends React.Component {
  render() {
    const { style, children } = this.props
    return <Text
      style={{
        color: 'red',
        ...style,
      }}
    >
      { children }
    </Text>
  }
}

export default ValidationText
