import React from 'react'
import { Text } from 'react-native'

class RequireSign extends React.Component {
  render() {
    const { style } = this.props
    return <Text
      style={{
        color: '#f00',
        fontSize: 10,
        ...style,
      }}
    >
      *
    </Text>
  }
}

export default RequireSign
