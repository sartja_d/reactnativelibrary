import React from 'react'
import { Text, View, StyleSheet, TouchableOpacity } from 'react-native'
import { Permissions, BarCodeScanner } from 'expo'

export default class BarcodeScanner extends React.Component {
  state = {
    hasCameraPermission: null,
    scanned: false,
  }

  async componentDidMount() {
    const { status } = await Permissions.askAsync(Permissions.CAMERA)
    this.setState({ hasCameraPermission: status === 'granted' })
  }

  handleBarCodeScanned = async ({ type, data }) => {
    this.setState({ scanned: true })
    this.props.onScan({ type, data })
  }

  cancelBarCodeScanned = async () => {
    this.setState({ scanned: false })
    this.props.onCancelScan()
  }

  render() {
    const { hasCameraPermission, scanned } = this.state

    if (hasCameraPermission === null) {
      return <Text>Requesting for camera permission</Text>
    }
    if (hasCameraPermission === false) {
      return <Text>No access to camera</Text>
    }
    return (
      <View style={styleSelf.cameraViewWrapper}>
        <View style={styleSelf.cameraView}>
          <BarCodeScanner
            onBarCodeScanned={scanned ? undefined : this.handleBarCodeScanned}
            style={{ ...StyleSheet.absoluteFillObject, width: 300, height: 300 }}
          />
        </View>
        <TouchableOpacity onPress={this.cancelBarCodeScanned}>
          <Text style={{ marginVertical: 20 }}>Close</Text>
        </TouchableOpacity>
      </View>
    )
  }
}

const styleSelf = StyleSheet.create({
  cameraViewWrapper: {
    position: 'absolute',
    left: 0,
    top: 0,
    right: 0,
    bottom: 0,
    zIndex: 100,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'rgba(52, 52, 52, 0.8)',
  },
  cameraView: {
    width: 300,
    height: 300,
  },
})
