import React from 'react'
import { StyleSheet } from 'react-native'
import RNPickerSelect from 'react-native-picker-select'
import Colors from '../constants/Colors'

class SelectOption extends React.Component {
  render() {
    const { style = {} } = this.props
    const iconContainer = {
      top: 20,
      right: 12,
      ...style.iconContainer
    }
    return <RNPickerSelect
      {...this.props}
      style={{
        ...pickerSelectStyles,
        iconContainer,
      }}
    />
  }
}

const pickerSelectStyles = StyleSheet.create({
  inputIOS: {
    fontSize: 16,
    paddingVertical: 16,
    paddingHorizontal: 8,
    borderWidth: 1,
    borderColor: Colors.grey,
    color: Colors.textColor,
    paddingRight: 30, // to ensure the text is never behind the icon
  },
  inputAndroid: {
    fontSize: 18,
    paddingHorizontal: 10,
    paddingVertical: 8,
    borderWidth: 1,
    borderColor: Colors.grey,
    color: Colors.textColor,
    paddingRight: 30, // to ensure the text is never behind the icon
  },
})

export default SelectOption
