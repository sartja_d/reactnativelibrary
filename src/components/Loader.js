import React from 'react'
import { View } from 'react-native'
import { Spinner } from 'native-base'

class Line extends React.Component {
  render() {
    return (
      <View style={{
        position: 'absolute',
        left: 0,
        top: 0,
        right: 0,
        bottom: 0,
        zIndex: 100,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'rgba(52, 52, 52, 0.8)',
      }}>
        <Spinner />
      </View>
    )
  }
}

export default Line
