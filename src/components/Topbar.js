import React from 'react'
import { View } from 'react-native'
import { Button } from 'native-base'
import { FontAwesome } from '@expo/vector-icons'
import Colors from '../constants/Colors'

class Topbar extends React.Component {
  render() {
    return <View
      style={{
        backgroundColor: Colors.grey,
        paddingHorizontal: 30,
        paddingVertical: 15,
      }}
    >
      <Button transparent onPress={this.props.onPress}>
        <FontAwesome name='navicon' color={Colors.white} size={24} />
      </Button>
    </View>
  }
}

export default Topbar
