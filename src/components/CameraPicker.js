import React from 'react'
import { StyleSheet, Text, View, TouchableOpacity } from 'react-native'
import { Camera, Permissions } from 'expo'
import { Ionicons } from '@expo/vector-icons'

export default class CameraPicker extends React.Component {
  state = {
    hasCameraPermission: null,
    type: Camera.Constants.Type.back,
    isTakingImage: false,
  }

  async componentDidMount() {
    const { status } = await Permissions.askAsync(Permissions.CAMERA);
    this.setState({ hasCameraPermission: status === 'granted' });
  }

  snap = async () => {
    setTimeout(() => {
      this.setState({ isTakingImage: true })
    }, 0)
    const photo = await this.camera.takePictureAsync()
    this.setState({ isTakingImage: false })
    this.props.onTakePhoto(photo)
  }

  render() {
    const { hasCameraPermission } = this.state;
    if (hasCameraPermission === null) {
      return <View />;
    } else if (hasCameraPermission === false) {
      return <Text>No access to camera</Text>
    }

    return (
      <View style={styleSelf.cameraView}>
        <Camera style={{ flex: 1 }} type={this.state.type} ref={(ref) => { this.camera = ref }}>
          <View
            style={{
              flex: 1,
              backgroundColor: 'transparent',
              flexDirection: 'row',
            }}>
            <View style={styleSelf.command}>
              <TouchableOpacity
                onPress={this.props.onClose}
                style={{ alignSelf: 'flex-end' }}
              >
                <View style={{ width: 60, height: 60 }}>
                  <Text style={{ color: '#fff', fontSize: 14, marginLeft: 30 }}>ปิด</Text>
                </View>
              </TouchableOpacity>
              <TouchableOpacity
                onPress={this.snap}
              >
                <View style={styleSelf.takePhotoBtn} />
              </TouchableOpacity>
              <TouchableOpacity
                onPress={() => {
                  this.setState({
                    type: this.state.type === Camera.Constants.Type.back
                      ? Camera.Constants.Type.front
                      : Camera.Constants.Type.back,
                  })
                }}
                style={{
                  alignSelf: 'flex-end',
                }}
              >
                <View style={{ width: 60, height: 60 }}>
                  <Ionicons style={styleSelf.flip} name='md-reverse-camera' color='#fff' size={40} />
                </View>
              </TouchableOpacity>
            </View>
          </View>
        </Camera>
      </View>
    )
  }
}


const styleSelf = StyleSheet.create({
  cameraView: {
    position: 'absolute',
    left: 0,
    top: 0,
    right: 0,
    bottom: 0,
    zIndex: 100,
  },
  command: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignSelf: 'flex-end',
  },
  takePhotoBtn: {
    width: 60,
    height: 60,
    backgroundColor: '#eee',
    borderWidth: 5,
    borderColor: '#fff',
    borderRadius: 30,
    margin: 10,
  },
  flip: {
    margin: 5,
  }
})
