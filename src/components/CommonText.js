import React from 'react'
import { Text } from 'react-native'
import Colors from '../constants/Colors'

class CommonText extends React.Component {
  render() {
    const { style, children } = this.props
    return <Text
      style={{
        color: Colors.textColor,
        fontSize: 13,
        ...style,
      }}
    >
      {children}
    </Text>
  }
}

export default CommonText
