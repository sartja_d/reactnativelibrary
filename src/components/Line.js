import React from 'react'
import { View } from 'react-native'
import Colors from '../constants/Colors'

class Line extends React.Component {
  render() {
    const { style } = this.props
    return <View
      style={{
        borderBottomColor: Colors.grey,
        borderBottomWidth: 1,
        ...style,
      }}
    />
  }
}

export default Line
