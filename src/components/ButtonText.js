import React from 'react'
import { Text } from 'react-native'
import Colors from '../constants/Colors'

class ButtonText extends React.Component {
  render() {
    const { style, children } = this.props
    return <Text
      style={{
        color: Colors.white,
        paddingHorizontal: 20,
        ...style,
      }}
    >
      {children}
    </Text>
  }
}

export default ButtonText
