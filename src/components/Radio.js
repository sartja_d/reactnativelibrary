import React from 'react'
import RadioForm, { RadioButton, RadioButtonInput, RadioButtonLabel } from 'react-native-simple-radio-button'
import Colors from '../constants/Colors'

class Radio extends React.Component {
  render() {
    const { radioList, onPress, value } = this.props
    return (
      <RadioForm formHorizontal={true}>
        {radioList.map((obj, i) => {
          return (
            <RadioButton labelHorizontal={true} key={i} >
              <RadioButtonInput
                obj={obj}
                index={i}
                isSelected={value === obj.value}
                onPress={onPress}
                buttonInnerColor={Colors.titleColor}
                buttonOuterColor={Colors.textColor}
                borderWidth={1}
                buttonSize={12}
              />
              <RadioButtonLabel
                obj={obj}
                index={i}
                labelHorizontal={true}
                onPress={onPress}
                labelWrapStyle={{ marginRight: 30 }}
              />
            </RadioButton>
          )
        })}
      </RadioForm>
    )
  }
}

export default Radio
