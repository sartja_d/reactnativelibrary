import React from 'react'
import {
  ScrollView,
  StyleSheet,
  TouchableOpacity,
  View,
} from 'react-native'
import {
  Button,
  Input,
  Item,
  Toast,
  Thumbnail,
} from 'native-base'
import { FontAwesome } from '@expo/vector-icons'
import { withNavigationFocus, NavigationEvents } from 'react-navigation'
import { Col, Row, Grid } from 'react-native-easy-grid'
import Dialog, { DialogContent } from 'react-native-popup-dialog'
import { Table, TableWrapper, Row as Thead, Cell } from 'react-native-table-component'
import dayjs from 'dayjs'
import { Formik } from 'formik'
import { find, get, isEmpty, map } from 'lodash'
import {
  BarcodeScanner,
  ButtonText,
  CommonText,
  Loader,
  Topbar,
  ValidationText,
} from '../components'
import Styles from '../constants/Styles'
import Colors from '../constants/Colors'
import { imageUri } from '../utils'
import { auth, db } from '../firebase'
// import console = require('console');

class BorrowScreen extends React.Component {
  state = {
    isLoading: false,
    isShowCommentModal: false,
    isShowScanner: false,
    isShowScanResult: false,
    scannerMode: 'borrow', // borrow, return
    bookList: [],
    newBookName: '',
    bookDetail: {
      code: '',
      name: '',
    },
    studentDetail: {
      avatar: '',
      nickname: '',
      borrowList: [],
    },
    schoolDetail: {
      id: '',
      name: '',
      province: '',
      city: '',
    },
    roomDetail: {
      year: '',
      level: '',
      room: '',
    }
  }

  userId = get(auth, 'currentUser.uid')
  studentId = ''

  onWillFocus = async () => {
    if (!this.userId) {
      this.props.navigation.navigate('Login')
    } else {
      this.setState({ isLoading: true })
      this.getBookList()
      this.studentId = this.props.navigation.getParam('studentId')
      const resStudent = await db.ref(`student_details/${this.studentId}`).once('value')
      const resRoom = await db.ref(`room_details/${get(resStudent.val(), 'room_id')}`).once('value')
      const resSchool = await db.ref(`school_details/${get(resRoom.val(), 'school_id')}`).once('value')
      this.setState({
        studentDetail: {
          ...this.state.studentDetail,
          ...resStudent.val(),
        },
        schoolDetail: {
          ...this.state.schoolDetail,
          ...resSchool.val(),
          id: get(resRoom.val(), 'school_id'),
          city: get(resSchool.val(), 'amphur'),
        },
        roomDetail: {
          ...this.state.roomDetail,
          ...resRoom.val(),
        },
        isLoading: false,
      })
    }
  }

  getBookList = async () => {
    const res = await db.ref('book_details').once('value')
    const list = map(res.val(), book => ({
      code: book.isbn,
      name: book.name
    }))
    this.setState({ bookList: list })
  }

  onBorrow = () => {
    this.setState({
      isShowScanner: true,
      scannerMode: 'borrow',
    })
  }

  onReturn = () => {
    this.setState({
      isShowScanner: true,
      scannerMode: 'return',
    })
  }

  onReturnManual = (book) => {
    this.setState({
      isShowCommentModal: true,
      scannerMode: 'return',
      bookDetail: {
        ...book,
      }
    })
  }

  onScanHandler = ({ type, data }) => {
    const { scannerMode, bookList } = this.state
    const bookObj = find(bookList, item => item.code === data) || {}
    const book = {
      code: bookObj.code || data,
      name: bookObj.name || '',
      date: dayjs().valueOf()
    }
    if (scannerMode === 'borrow') {
      this.borrowAction(book)
    } else {
      this.returnAction(book)
    }
  }

  onAddNewBook = async (book) => {
    const { code, name } = book
    const { studentDetail } = this.state
    const updatedBorrowList = studentDetail.borrowList.map((item) => {
      if (item.code === code) {
        return {
          ...item,
          name,
        }
      }
      return item
    })
    await db.ref(`book_details/${code}`).set({ isbn: code, name })
    await db.ref(`student_details/${this.studentId}`).update({ borrowList: updatedBorrowList })
    this.getBookList()
    this.setState({
      isShowScanResult: false,
      studentDetail: {
        ...studentDetail,
        borrowList: updatedBorrowList,
      }
    })
  }

  borrowAction = async (book) => {
    const { studentDetail } = this.state
    const existBook = find(studentDetail.borrowList, item => item.code === book.code)
    if (isEmpty(existBook)) {
      const newBorrowList = [...studentDetail.borrowList, book]
      const borrowRecord = {
        bookCode: book.code,
        bookName: book.name,
        date: book.date,
        type: 'borrow',
        teacherId: this.userId,
        studentId: this.studentId,
        roomId: studentDetail.room_id,
        comment: '',
      }
      // kei adjust : add new data and keep id
      // const newBorrowListK = [...studentDetail.borrowList, book]
      const borrowRecordK = {
        bookCode: book.code,
        bookName: book.name,
        dateBorrow: book.date,
        dateReturn: '',
        type: 'borrow',
        teacherIdBorrow: this.userId,
        teacherIdReturn: '',
        studentId: this.studentId,
        roomId: studentDetail.room_id,
        comment: '',
      }
      book.BorrowId=await db.ref('borrow_history_k').push(borrowRecordK).key
      await db.ref(`student_details/${this.studentId}`).update({ borrowList: [...studentDetail.borrowList, book] })
      // kei adjust
      // await db.ref(`student_details/${this.studentId}`).update({ borrowList: newBorrowList })
      await db.ref('borrow_history').push(borrowRecord)
      this.setState({
        isShowScanner: false,
        isShowScanResult: true,
        bookDetail: { ...book },
        studentDetail: {
          ...studentDetail,
          borrowList: newBorrowList,
        }
      })
    } else {
      Toast.show({
        text: 'คุณมีหนังสือเล่มนี้ในรายการอยู่แล้ว',
        buttonText: 'close',
        type: 'warning',
        position: 'top',
        duration: 3000
      })
    }
  }

  returnAction = async (book) => {
    const { studentDetail } = this.state
    const remainBorrowList = studentDetail.borrowList.filter(item => item.code !== book.code)
    const returnRecord = {
      bookCode: book.code,
      bookName: book.name,
      date: book.date,
      type: 'return',
      teacherId: this.userId,
      studentId: this.studentId,
      roomId: studentDetail.room_id,
      comment: '',
    }

    if (studentDetail.borrowList.length !== remainBorrowList.length) {

      // k adjust database borrow_history_k : find id borrow
      const remainBorrowList_k = studentDetail.borrowList.filter(item => item.code == book.code)
      const remainBorrowList_k1=remainBorrowList_k[0].BorrowId
      await db.ref(`borrow_history_k/${remainBorrowList_k1}`).update({dateReturn: book.date,teacherIdReturn: this.userId,type: 'return' })
      // k adjust database borrow_history_k

      await db.ref(`student_details/${this.studentId}`).update({ borrowList: remainBorrowList })
      await db.ref('borrow_history').push(returnRecord)
      this.setState({
        isShowScanner: false,
        isShowScanResult: true,
        bookDetail: { ...book },
        studentDetail: {
          ...studentDetail,
          borrowList: remainBorrowList,
        }
      })
    } else {
      Toast.show({
        text: 'คุณไม่มีหนังสือเล่มนี้ค้างในรายการ',
        buttonText: 'close',
        type: 'warning',
        position: 'top',
        duration: 3000
      })
    }
  }

  onCancelScanHandler = () => {
    this.setState({ isShowScanner: false })
  }

  tableDateFormatter = (list) => {
    return list.map(item => [
      dayjs(item.date).format('DD/MM/YYYY'),
      item.name || item.code,
      { code: item.code, name: item.name },
    ])
  }

  validate = (values) => {
    const errors = {}
    if (!values.comment) {
      errors.comment = 'กรุณาระบุเหตุผล'
    }
    return errors
  }

  onSubmit = async (values) => {
    const { bookDetail, studentDetail } = this.state
    this.setState({
      isLoading: true,
    })

     // k adjust database borrow_history_k
     const remainBorrowList_k = studentDetail.borrowList.filter(item => item.code == bookDetail.code)
     const remainBorrowList_k1=remainBorrowList_k[0].BorrowId
     await db.ref(`borrow_history_k/${remainBorrowList_k1}`).update({comment: values.comment,dateReturn: dayjs().valueOf(),teacherIdReturn: this.userId,type: 'return' })
     // k adjust database borrow_history_k

    const remainBorrowList = studentDetail.borrowList.filter(item => item.code !== bookDetail.code)
    const returnRecord = {
      bookCode: bookDetail.code,
      bookName: bookDetail.name,
      date: dayjs().valueOf(),
      type: 'return',
      teacherId: this.userId,
      studentId: this.studentId,
      roomId: studentDetail.room_id,
      comment: values.comment,
    }
    await db.ref(`student_details/${this.studentId}`).update({ borrowList: remainBorrowList })
    await db.ref('borrow_history').push(returnRecord)
    this.setState({
      isLoading: false,
      isShowCommentModal: false,
      isShowScanResult: true,
      studentDetail: {
        ...studentDetail,
        borrowList: remainBorrowList,
      }
    })
  }

  render() {
    const { navigation } = this.props
    const {
      isLoading,
      isShowCommentModal,
      isShowScanner,
      isShowScanResult,
      scannerMode,
      newBookName,
      bookDetail,
      studentDetail,
      schoolDetail,
      roomDetail,
    } = this.state
    return (
      <View style={Styles.container}>
        <NavigationEvents
          onWillFocus={this.onWillFocus}
        />
        { isShowScanner &&
          <BarcodeScanner
            onScan={this.onScanHandler}
            onCancelScan={this.onCancelScanHandler}
          />
        }
        {isLoading && <Loader /> }
        <Topbar onPress={() => navigation.toggleDrawer()} />
        <ScrollView style={Styles.container}>
          <View style={styleSelf.borrowBox}>
            <Grid>
              <Row>
                <Col style={{ width: 100 }}>
                  <Thumbnail
                    square
                    style={{ height: 100, width: 100 }}
                    source={imageUri(studentDetail.avatar)}
                  />
                </Col>
                <Col>
                  <View style={styleSelf.profileAreaDetail}>
                    <Row style={Styles.mb10}>
                      <Col style={{ width: 50 }}>
                        <CommonText style={Styles.bold}>ชื่อเล่น:</CommonText>
                      </Col>
                      <Col>
                        <CommonText>{studentDetail.nickname}</CommonText>
                      </Col>
                    </Row>
                    <Row style={Styles.mb10}>
                      <Col style={{ width: 60 }}>
                        <CommonText style={Styles.bold}>ระดับชั้น:</CommonText>
                      </Col>
                      <Col>
                        <CommonText>{roomDetail.level} ห้อง {roomDetail.room}</CommonText>
                      </Col>
                    </Row>
                    <Row>
                      <Col style={{ width: 60 }}>
                        <CommonText style={Styles.bold}>โรงเรียน:</CommonText>
                      </Col>
                      <Col>
                        <CommonText>{schoolDetail.name}</CommonText>
                      </Col>
                    </Row>
                  </View>
                </Col>
              </Row>
            </Grid>
            <Grid>
              <Row style={{ marginTop: 20, marginBottom: 40 }}>
                <Col size={40} style={Styles.mr20}>
                  <TouchableOpacity onPress={this.onBorrow}>
                    <View style={styleSelf.tabBtn}>
                      <CommonText style={{ textAlign: 'center' }}>ยืม</CommonText>
                    </View>
                  </TouchableOpacity>
                </Col>
                <Col size={40}>
                  <TouchableOpacity onPress={this.onReturn}>
                    <View style={{ ...styleSelf.tabBtn, backgroundColor: '#ce3c3e' }}>
                      <CommonText style={{ textAlign: 'center' }}>คืน</CommonText>
                    </View>
                  </TouchableOpacity>
                </Col>
              </Row>
              <Row>
                <Col>
                  <CommonText style={styleSelf.tableTitle}>รายการยืม-คืนนิทาน</CommonText>
                </Col>
              </Row>
              <Row style={Styles.my20}>
                <Col>
                  <Table borderStyle={{ borderColor: '#93c5b4' }}>
                    <Thead
                      data={['วัน/เดือน/ปี', 'ชื่อหนังสือ', '']}
                      style={styleSelf.thead}
                      textStyle={{ textAlign: 'center' }}
                      flexArr={[4, 4, 1]}
                    />
                    {
                      this.tableDateFormatter(studentDetail.borrowList).map((rowData, index) => (
                        <TableWrapper key={index} style={{ flexDirection: 'row', backgroundColor: '#e9edf0' }}>
                          {
                            rowData.map((cellData, cellIndex) => (
                              <Cell
                                key={cellIndex}
                                flex={cellIndex === 2 ? 1 : 4 }
                                textStyle={{ margin: 10 }}
                                data={cellIndex === 2 ? (
                                  <TouchableOpacity onPress={() => this.onReturnManual(cellData)}>
                                    <FontAwesome
                                      name='times-circle'
                                      color={'#fc0502'}
                                      size={24}
                                      style={{ textAlign: 'center' }}
                                    />
                                  </TouchableOpacity>
                                ) : cellData}
                              />
                            ))
                          }
                        </TableWrapper>
                      ))
                    }
                  </Table>
                </Col>
              </Row>
            </Grid>
            <Dialog
              width={0.8}
              visible={isShowScanResult}
              rounded
            >
              <DialogContent style={{ backgroundColor: Colors.grey }}>
                <View style={{ paddingHorizontal: 20, paddingVertical: 40 }}>
                  <FontAwesome
                    name='check'
                    color={scannerMode === 'borrow' ? '#4dad4a' : '#ce3c3e'}
                    size={72}
                    style={{ textAlign: 'center' }}
                  />
                  <View style={styleSelf.scanResult}>
                    <CommonText style={{ textAlign: 'center', marginBottom: 10 }}>{`น้อง ${studentDetail.nickname} ${scannerMode === 'borrow' ? 'ยืม' : 'คืน'}หนังสือ`}</CommonText>
                    <CommonText style={{ textAlign: 'center', marginBottom: 10 }}>{bookDetail.name || bookDetail.code}</CommonText>
                    <CommonText style={{ textAlign: 'center' }}>สำเร็จ</CommonText>
                  </View>
                  { scannerMode === 'borrow' && !bookDetail.name && (
                    <Item regular style={{ backgroundColor: Colors.white, marginBottom: 10 }}>
                      <Input
                        placeholder='ระบุชื่อหนังสือ'
                        onChangeText={text => this.setState({ newBookName: text })}
                      />
                    </Item>
                  )}
                  <Grid>
                    <Row>
                      <Col>
                        <Button
                          block
                          onPress={() => this.setState({ isShowScanResult: false })}
                        >
                          <ButtonText>ปิด</ButtonText>
                        </Button>
                      </Col>
                      { scannerMode === 'borrow' && !bookDetail.name && (
                        <Col style={{ marginLeft: 10 }}>
                          <Button
                            block
                            success
                            onPress={() => this.onAddNewBook({ code: bookDetail.code, name: newBookName }) }
                          >
                            <ButtonText>บันทึก</ButtonText>
                          </Button>
                        </Col>
                      )}
                    </Row>
                  </Grid>
                </View>
              </DialogContent>
            </Dialog>
            <Dialog
              width={0.8}
              visible={isShowCommentModal}
              rounded
            >
              <DialogContent>
                <Formik
                  initialValues={{ comment: '' }}
                  validate={this.validate}
                  onSubmit={this.onSubmit}
                >
                  {(formProps) => {
                    const {
                      errors,
                      handleChange,
                      handleBlur,
                      handleSubmit,
                      touched,
                      values,
                    } = formProps
                    return (
                      <View style={{ padding: 20 }}>
                        <CommonText style={{ fontSize: 20, textAlign: 'center', marginBottom: 10 }}>ระบุเหตุผล</CommonText>
                        <Item regular>
                          <Input
                            multiline={true}
                            numberOfLines={5}
                            style={{ height: 150, justifyContent: 'flex-start' }}
                            onChangeText={handleChange('comment')}
                            onBlur={handleBlur('comment')}
                            value={values.comment}
                          />
                        </Item>
                        { errors.comment && touched.comment && <ValidationText>{errors.comment}</ValidationText>}
                        <View style={{ flexDirection: 'row', justifyContent: 'flex-end', marginVertical: 10 }}>
                          <Button
                            block
                            style={Styles.mr10}
                            onPress={() => this.setState({ isShowCommentModal: false })}
                          >
                            <ButtonText>ปิด</ButtonText>
                          </Button>
                          <Button
                            danger
                            block
                            onPress={handleSubmit}
                          >
                            <ButtonText>ยืนยัน</ButtonText>
                          </Button>
                        </View>
                      </View>
                    )
                  }}
                </Formik>
              </DialogContent>
            </Dialog>
          </View>
        </ScrollView>
      </View>
    )
  }
}

export default withNavigationFocus(BorrowScreen)

const styleSelf = StyleSheet.create({
  borrowBox: {
    backgroundColor: Colors.greyLight,
    paddingVertical: 50,
    paddingHorizontal: 20,
  },
  profileArea: {
    flexDirection: 'row',
  },
  profileAreaDetail: {
    paddingHorizontal: 20,
    paddingVertical: 10,
  },
  textWrap: {
    flex: 1,
    flexWrap: 'wrap',
  },
  tabBtn: {
    backgroundColor: '#16c787',
    borderWidth: 2,
    borderRadius: 3,
    padding: 10,
  },
  tableTitle: {
    marginVertical: 30,
    fontWeight: 'bold',
    fontSize: 20,
    textAlign: 'center',
  },
  thead: {
    height: 40,
    backgroundColor: Colors.grey,
  },
  scanResult: {
    paddingVertical: 20,
    marginVertical: 20,
    borderTopWidth: 1,
    borderTopColor: Colors.greyLight,
    borderBottomWidth: 1,
    borderBottomColor: Colors.greyLight,
  }
})
