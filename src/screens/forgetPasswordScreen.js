import React from 'react'
import {
  ScrollView,
  StyleSheet,
  View,
} from 'react-native'
import {
  Button,
  Input,
  Item,
  Toast,
} from 'native-base'
import { Formik } from 'formik'
import { ButtonText, CommonText, Loader, ValidationText } from '../components'
import { auth, db } from '../firebase'
import Styles from '../constants/Styles'
import Colors from '../constants/Colors'

class forgetPasswordScreen extends React.Component {
  state = {
    isLoading: false,
  }

  validate = (values) => {
    const errors = {}
    if (!values.email) {
      errors.email = 'กรุณากรอกอีเมล'
    }
    return errors
  }

  onSubmit = (values) => {
    this.setState({ isLoading: true })
    auth.sendPasswordResetEmail(values.email)
    .then(() => {
      this.setState({ isLoading: false })
      Toast.show({
        text: 'อีเมล์ถูกส่งแล้ว กรุณาตรวจสอบ',
        buttonText: 'close',
        type: 'success',
        position: 'top',
        duration: 5000
      })
    }).catch((error) => {
      this.setState({ isLoading: false })
      Toast.show({
        text: error.message,
        buttonText: 'close',
        type: 'danger',
        position: 'top',
        duration: 5000
      })
    })
  }

  render() {
    const { isLoading } = this.state
    return (
      <View style={Styles.container}>
        { isLoading && <Loader /> }
        <ScrollView style={Styles.container} contentContainerStyle={Styles.contentContainer}>
          <Formik
            initialValues={{
              email: '',
            }}
            validate={this.validate}
            onSubmit={this.onSubmit}
          >
            {(formProps) => {
              const {
                errors,
                handleChange,
                handleBlur,
                handleSubmit,
                touched,
                values,
              } = formProps
              return (
                <View style={styleSelf.RegisterBox}>
                  <CommonText style={styleSelf.title}>รีเซ็ตรหัสผ่าน!</CommonText>
                  <View style={Styles.my20}>
                    <Item regular>
                      <Input
                        placeholder='อีเมลผู้ใช้งาน'
                        placeholderTextColor={Colors.subTitleColor}
                        autoCapitalize='none'
                        onChangeText={handleChange('email')}
                        onBlur={handleBlur('email')}
                        value={values.email}
                      />
                    </Item>
                    { errors.email && touched.email && <ValidationText>{errors.email}</ValidationText>}
                  </View>
                  <Button
                    block
                    style={{ marginTop: 20, marginBottom: 100 }}
                    onPress={handleSubmit}
                  >
                    <ButtonText>ส่งอีเมล์</ButtonText>
                  </Button>
                </View>
              )
            }}
          </Formik>
        </ScrollView>
      </View>
    )
  }
}

export default forgetPasswordScreen

const styleSelf = StyleSheet.create({
  RegisterBox: {
    backgroundColor: Colors.white,
    margin: 30,
    paddingVertical: 50,
    paddingHorizontal: 30,
    borderRadius: 50,
  },
  title: {
    fontSize: 20,
    color: Colors.titleColor,
    textAlign: 'center'
  },
})
