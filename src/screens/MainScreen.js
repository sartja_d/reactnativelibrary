import React from 'react'
import {
  Alert,
  Dimensions,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native'
import {
  Badge,
  Input,
  Thumbnail,
} from 'native-base'
import { FontAwesome } from '@expo/vector-icons'
import { Permissions } from 'expo'
import { withNavigationFocus, NavigationEvents } from 'react-navigation'
import { Col, Row, Grid } from 'react-native-easy-grid'
import { get, map } from 'lodash'
import Dialog, { DialogButton, DialogContent, DialogFooter, DialogTitle } from 'react-native-popup-dialog'
import { CommonText, Loader, Topbar } from '../components'
import Styles from '../constants/Styles'
import Colors from '../constants/Colors'
import { imageUri, removeImage } from '../utils'
import { auth, db } from '../firebase'

class MainScreen extends React.Component {
  state = {
    isLoading: false,
    isShowDeleteStudentModal: false,
    studentList: [],
    targetDelete: {
      id: '',
      name: '',
    },
    teacherDetail: {
      avatar: '',
      gender: '',
      name: '',
      surname: '',
      nickname: '',
      email: '',
    },
    schoolDetail: {
      id: '',
      name: '',
      province: '',
      city: '',
    },
    roomDetail: {
      year: '',
      level: '',
      room: '',
    }
  }

  userId = get(auth, 'currentUser.uid')

  onWillFocus = async () => {
    const { navigation } = this.props
    if (!this.userId) {
      navigation.navigate('Login')
    } else {
      const permission = await Permissions.getAsync(Permissions.CAMERA_ROLL, Permissions.CAMERA)
      if (permission.status !== 'granted') {
        const newPermission = await Permissions.askAsync(Permissions.CAMERA_ROLL, Permissions.CAMERA)
        if (newPermission.status !== 'granted') {
          Alert.alert('Some features maybe not work.')
        }
      }
      this.setState({ isLoading: true })

      // get teacher & school & room detail
      const resTeacher = await db.ref(`teacher_details/${this.userId}`).once('value')
      if (!get(resTeacher.val(), 'name')) {
        const profileData = navigation.getParam('profileData')
        navigation.navigate('TeacherProfile', { profileData })
      }
      const resRoom = await db.ref(`room_details/${get(resTeacher.val(), 'room_id')}`).once('value')
      const resSchool = await db.ref(`school_details/${get(resRoom.val(), 'school_id')}`).once('value')
      this.setState({
        teacherDetail: {
          ...this.state.teacherDetail,
          ...resTeacher.val(),
        },
        schoolDetail: {
          ...this.state.schoolDetail,
          ...resSchool.val(),
          id: get(resRoom.val(), 'school_id'),
          city: get(resSchool.val(), 'amphur'),
        },
        roomDetail: {
          ...this.state.roomDetail,
          ...resRoom.val(),
        },
      })

      // get student list of room id
      const res = await db.ref('student_details').orderByChild('room_id').equalTo(this.state.teacherDetail.room_id).once('value')
      if (res.val()) {
        const list = map(res.val(), (student, key) => ({ ...student, id: key }))
        this.setState({
          studentList: [...list],
          isLoading: false,
        })
      } else {
        this.setState({
          studentList: [],
          isLoading: false,
        })
      }
    }
  }

  openConfirmation = ({ studentId, studentName }) => {
    this.setState({
      isShowDeleteStudentModal: true,
      targetDelete: {
        id: studentId,
        name: studentName,
      },
    })
  }

  deleteStudent = () => {
    const { studentList, targetDelete } = this.state
    if (targetDelete.name === 'ทั้งหมด') {
      studentList.forEach((student) => {
        db.ref(`student_details/${student.id}`).remove()
        if (student.avatar) {
          removeImage(`images/image_student/${student.id}/avatar.jpg`)
        }
      })
      this.setState({
        isShowDeleteStudentModal: false,
        studentList: []
      })
    } else {
      db.ref(`student_details/${targetDelete.id}`).remove()
      const stutendDeleted = studentList.find(item => item.id === targetDelete.id)
      if (stutendDeleted.avatar) {
        removeImage(`images/image_student/${targetDelete.id}/avatar.jpg`)
      }
      const newStudentList = studentList.filter(student => student.id !== targetDelete.id)
      this.setState({
        isShowDeleteStudentModal: false,
        studentList: newStudentList
      })
    }
  }

  render() {
    const { navigation } = this.props
    const { isLoading, isShowDeleteStudentModal, studentList, teacherDetail, schoolDetail, roomDetail } = this.state
    return (
      <View style={Styles.container}>
        <NavigationEvents
          onWillFocus={this.onWillFocus}
        />
        {isLoading && <Loader /> }
        <Topbar onPress={() => navigation.toggleDrawer()} />
        <ScrollView style={Styles.container}>
          <Grid>
            <Row style={styleSelf.topBox}>
              <Col >
                <View style={styleSelf.avatar}>
                  <Thumbnail large source={imageUri(teacherDetail.avatar)} />
                </View>
                <CommonText style={styleSelf.name}>{ teacherDetail.name }</CommonText>
                <CommonText style={styleSelf.text}>ครูประจำชั้น { roomDetail.level } ห้อง { roomDetail.room }</CommonText>
                <CommonText style={styleSelf.text}>โรงเรียน { schoolDetail.name }</CommonText>
              </Col>
            </Row>
            <Row style={styleSelf.bodyBox}>
              <Col style={{ ...Styles.centerElement, width }}>
                <Row style={{ marginTop: 20 }}>
                  <Col size={25}>
                    <CommonText style={styleSelf.inputText}>ปีการศึกษา</CommonText>
                  </Col>
                  <Col size={75}>
                    <Input
                      editable={false}
                      value={roomDetail.year}
                    />
                  </Col>
                </Row>
              </Col>
              <Col style={{ ...Styles.centerElement, width: width / 2 }}>
                <Row style={{ marginBottom: 20 }}>
                  <Col>
                    <CommonText style={styleSelf.inputText}>ห้อง</CommonText>
                  </Col>
                  <Col>
                    <Input
                      editable={false}
                      value={roomDetail.room}
                    />
                  </Col>
                </Row>
              </Col>
              <Col style={{ ...Styles.centerElement, width: width / 2 }}>
                {/* <Row>
                  <Button danger style={Styles.mt20} onPress={() => this.openConfirmation({ studentId: '', studentName: 'ทั้งหมด' })}>
                    <ButtonText>ลบทั้งหมด</ButtonText>
                  </Button>
                </Row> */}
                <Row>
                  <CommonText>จำนวน {studentList.length} คน</CommonText>
                </Row>
              </Col>
              <Col style={styleSelf.addAreaWrapper}>
                <TouchableOpacity onPress={() => navigation.navigate('StudentProfile')}>
                  <View style={styleSelf.addArea}>
                    <FontAwesome name='plus' color={Colors.white} size={26} />
                  </View>
                </TouchableOpacity>
                <CommonText style={{ textAlign: 'center', marginTop: 10 }}>เพิ่มนักเรียน</CommonText>
              </Col>
              {
                studentList.map((student) => {
                  return (
                    <Col style={styleSelf.addAreaWrapper} key={student.id}>
                      <View style={{ position: 'relative' }}>
                        <TouchableOpacity
                          onPress={() => navigation.navigate('Borrow', { studentId: student.id })}
                        >
                          <Thumbnail square style={{ height: 100, width: 100 }} source={imageUri(student.avatar)} />
                        </TouchableOpacity>
                        <Badge warning style={styleSelf.noti}>
                          <CommonText style={{ textAlign: 'center', color: '#fff' }}>{get(student, 'borrowList.length') || 0}</CommonText>
                        </Badge>
                      </View>
                      <CommonText style={{ textAlign: 'center', marginBottom: 5 }}>{student.name}</CommonText>
                      <View style={{ flexDirection: 'row' }}>
                        <TouchableOpacity
                          style={{ flexDirection: 'row', ...Styles.centerElement }}
                          onPress={() => navigation.navigate('StudentProfile', { studentId: student.id })}
                        >
                          <FontAwesome name='pencil' style={Styles.mr10} />
                          <CommonText>แก้ไข</CommonText>
                        </TouchableOpacity>
                        <CommonText style={Styles.mx10}>|</CommonText>
                        <TouchableOpacity
                          style={{ flexDirection: 'row', ...Styles.centerElement }}
                          onPress={() => this.openConfirmation({ studentId: student.id, studentName: student.name })}
                        >
                          <FontAwesome name='trash-o' style={Styles.mr10} />
                          <CommonText>ลบ</CommonText>
                        </TouchableOpacity>
                      </View>
                    </Col>
                  )
                })
              }
            </Row>
          </Grid>
          <Dialog
            width={0.8}
            visible={isShowDeleteStudentModal}
            rounded
            dialogTitle={
              <DialogTitle
                title='ลบนักเรียน'
                hasTitleBar={false}
              />
            }
            footer={
              <DialogFooter>
                <DialogButton
                  text='ยกเลิก'
                  bordered
                  onPress={() => this.setState({ isShowDeleteStudentModal: false })}
                />
                <DialogButton
                  text='ลบ'
                  bordered
                  onPress={this.deleteStudent}
                />
              </DialogFooter>
            }
          >
            <DialogContent>
              <Text>{`คุณต้องการลบข้อมูลของ ${this.state.targetDelete.name}`} ?</Text>
            </DialogContent>
          </Dialog>
        </ScrollView>
      </View>
    )
  }
}

export default withNavigationFocus(MainScreen)

const width = Dimensions.get('window').width

const styleSelf = StyleSheet.create({
  topBox: {
    backgroundColor: Colors.grey,
    height: 250,
  },
  bodyBox: {
    backgroundColor: '#f8f9fc',
    flexWrap: 'wrap',
  },
  avatar: {
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 20,
  },
  name: {
    fontWeight: 'bold',
    fontSize: 20,
    textAlign: 'center',
    marginBottom: 10,
  },
  text: {
    textAlign: 'center',
    marginBottom: 10,
    fontSize: 14,
  },
  inputText: {
    fontSize: 14,
    fontWeight: 'bold',
    paddingTop: 13,
    marginRight: 10,
    textAlign: 'right'
  },
  addAreaWrapper: {
    width: width / 2,
    justifyContent: 'center',
    alignItems: 'center',
    borderWidth: 1,
    borderColor: Colors.white,
    padding: 20,
    backgroundColor: Colors.greyLight,
  },
  addArea: {
    width: 100,
    height: 100,
    backgroundColor: Colors.grey,
    justifyContent: 'center',
    alignItems: 'center',
  },
  noti: {
    width: 25,
    height: 25,
    position: 'absolute',
    top: -10,
    right: -10,
  }
})
