import React from 'react'
import {
  ScrollView,
  StyleSheet,
  View,
} from 'react-native'
import {
  Button,
  Input,
  Item,
  Toast,
} from 'native-base'
import { Formik } from 'formik'
import { ButtonText, CommonText, Loader, ValidationText } from '../components'
import { auth, db } from '../firebase'
import Styles from '../constants/Styles'
import Colors from '../constants/Colors'

class RegisterScreen extends React.Component {
  state = {
    isLoading: false,
  }

  validate = (values) => {
    const errors = {}
    if (!values.email) {
      errors.email = 'กรุณากรอกอีเมล'
    }
    if (!values.password) {
      errors.password = 'กรุณากรอกรหัสผ่าน'
    }
    if (!values.rePassword) {
      errors.rePassword = 'กรุณากรอกยืนยันรหัสผ่าน'
    }
    if (values.password !== values.rePassword) {
      errors.rePassword = 'รหัสผ่านและยืนยันรหัสผ่าน ไม่เหมือนกัน'
    }
    return errors
  }

  onSubmit = (values) => {
    this.setState({ isLoading: true })
    auth.createUserWithEmailAndPassword(values.email, values.password)
    .then((res) => {
      const { email, uid } = res.user
      db.ref(`teacher_details/${uid}`).set({
        email,
      })
      this.setState({ isLoading: false })
      this.props.navigation.navigate('TeacherProfile')
    })
    .catch((error) => {
      this.setState({ isLoading: false })
      Toast.show({
        text: error.message,
        buttonText: 'close',
        type: 'danger',
        position: 'top',
        duration: 5000
      })
    })
  }

  render() {
    const { isLoading } = this.state
    return (
      <View style={Styles.container}>
        { isLoading && <Loader /> }
        <ScrollView style={Styles.container} contentContainerStyle={Styles.contentContainer}>
          <Formik
            initialValues={{
              email: '',
              password: '',
              rePassword: '',
            }}
            validate={this.validate}
            onSubmit={this.onSubmit}
          >
            {(formProps) => {
              const {
                errors,
                handleChange,
                handleBlur,
                handleSubmit,
                touched,
                values,
              } = formProps
              return (
                <View style={styleSelf.RegisterBox}>
                  <CommonText style={styleSelf.title}>สร้างบัญชีผู้ใช้งานใหม่!</CommonText>
                  <View style={Styles.my20}>
                    <Item regular>
                      <Input
                        placeholder='อีเมลผู้ใช้งาน'
                        placeholderTextColor={Colors.subTitleColor}
                        autoCapitalize='none'
                        onChangeText={handleChange('email')}
                        onBlur={handleBlur('email')}
                        value={values.email}
                      />
                    </Item>
                    { errors.email && touched.email && <ValidationText>{errors.email}</ValidationText>}
                  </View>
                  <View style={Styles.mb20}>
                  <ValidationText>กรุณากรอกอย่างน้อย 6 ตัว</ValidationText>
                    <Item regular>
                      <Input
                        placeholder='รหัสผ่าน'
                        placeholderTextColor={Colors.subTitleColor}
                        secureTextEntry
                        onChangeText={handleChange('password')}
                        onBlur={handleBlur('password')}
                        value={values.password}
                      />
                    </Item>
                    { errors.password && touched.password && <ValidationText>{errors.password}</ValidationText>}
                  </View>
                  <View style={Styles.mb20}>
                    <Item regular>
                      <Input
                        placeholder='ยืนยันรหัสผ่าน'
                        placeholderTextColor={Colors.subTitleColor}
                        secureTextEntry
                        onChangeText={handleChange('rePassword')}
                        onBlur={handleBlur('rePassword')}
                        value={values.rePassword}
                      />
                    </Item>
                    { errors.rePassword && touched.rePassword && <ValidationText>{errors.rePassword}</ValidationText>}
                  </View>
                  <Button
                    block
                    style={{ marginTop: 20, marginBottom: 100 }}
                    onPress={handleSubmit}
                  >
                    <ButtonText>เข้าสู่ระบบ</ButtonText>
                  </Button>
                </View>
              )
            }}
          </Formik>
        </ScrollView>
      </View>
    )
  }
}

export default RegisterScreen

const styleSelf = StyleSheet.create({
  RegisterBox: {
    backgroundColor: Colors.white,
    margin: 30,
    paddingVertical: 50,
    paddingHorizontal: 30,
    borderRadius: 50,
  },
  title: {
    fontSize: 20,
    color: Colors.titleColor,
    textAlign: 'center'
  },
})
