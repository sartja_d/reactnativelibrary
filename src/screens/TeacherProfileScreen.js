import React from 'react'
import {
  KeyboardAvoidingView,
  ScrollView,
  StyleSheet,
  TouchableOpacity,
  View,
} from 'react-native'
import {
  Button,
  Input,
  Item,
  Thumbnail,
} from 'native-base'
import { ImagePicker } from 'expo'
import { FontAwesome } from '@expo/vector-icons'
import { withNavigationFocus, NavigationEvents } from 'react-navigation'
import { find, isEmpty, get, map } from 'lodash'
import { Col, Row, Grid } from 'react-native-easy-grid'
import { Formik } from 'formik'
import ModalFilterPicker from 'react-native-modal-filter-picker'
import {
  ButtonText,
  CameraPicker,
  CommonText,
  ImageViewer,
  Loader,
  Radio,
  RequireSign,
  SelectOption,
  Topbar,
  ValidationText,
} from '../components'
import { auth, db } from '../firebase'
import { uploadImage } from '../utils'
import Styles from '../constants/Styles'
import Colors from '../constants/Colors'

class TeacherProfileScreen extends React.Component {
  state = {
    isChangeAvatar: false,
    isShowSchoolListModal: false,
    isShowCamera: false,
    isShowImage: false,
    isLoading: false,
    schoolList: [],
    teacherDetail: {
      avatar: '',
      gender: '',
      name: '',
      surname: '',
      nickname: '',
      email: '',
    },
    schoolDetail: {
      id: '',
      name: '',
      province: '',
      city: '',
    },
    roomDetail: {
      year: '',
      level: '',
      room: '',
    }
  }
  userId = get(auth, 'currentUser.uid')

  onWillFocus = async () => {
    if (!this.userId) {
      this.props.navigation.navigate('Login')
    } else {
      this.setState({ isLoading: true })
      this.getSchoolList()

      // get profile form google & facebook when register by social
      const profileData = this.props.navigation.getParam('profileData')
      if (profileData) {
        this.setState({
          teacherDetail: {
            ...this.state.teacherDetail,
            name: profileData.name,
            surname: profileData.surname,
            email: profileData.email,
          },
          isLoading: false,
        })
      } else {
        const resTeacher = await db.ref(`teacher_details/${this.userId}`).once('value')
        const resRoom = await db.ref(`room_details/${get(resTeacher.val(), 'room_id')}`).once('value')
        const resSchool = await db.ref(`school_details/${get(resRoom.val(), 'school_id')}`).once('value')

        this.setState({
          teacherDetail: {
            ...this.state.teacherDetail,
            ...resTeacher.val(),
          },
          schoolDetail: {
            ...this.state.schoolDetail,
            ...resSchool.val(),
            id: get(resRoom.val(), 'school_id'),
            city: get(resSchool.val(), 'amphur'),
          },
          roomDetail: {
            ...this.state.roomDetail,
            ...resRoom.val(),
          },
          isLoading: false,
        })
      }
    }
  }

  getSchoolList = async () => {
    const res = await db.ref('school_details').once('value')
    const list = map(res.val(), (school, key) => ({
      key: { id: key, name: school.name, province: school.province, city: school.amphur },
      label: school.name
    }))
    this.setState({ schoolList: list })
  }

  takePhotoHandler = async (photo, setFieldValue) => {
    setFieldValue('teacherDetail.avatar', photo)
    this.setState({ isChangeAvatar: true, isShowCamera: false })
  }

  pickImage = async (setFieldValue) => {
    let result = await ImagePicker.launchImageLibraryAsync({
      mediaTypes: 'Images',
      allowsEditing: true,
      aspect: [4, 3],
    })
    if (!result.cancelled) {
      setFieldValue('teacherDetail.avatar', result.uri)
      this.setState({ isChangeAvatar: true })
    }
  }

  onSelectSchoolList = (pickObj, setFieldValue) => {
    setFieldValue('schoolDetail.id', pickObj.id)
    setFieldValue('schoolDetail.name', pickObj.name)
    setFieldValue('schoolDetail.province', pickObj.province)
    setFieldValue('schoolDetail.city', pickObj.city)
    this.setState({
      isShowSchoolListModal: false,
    })
  }

  checkExistRoom = async (values) => {
    const { schoolDetail, roomDetail } = values

    // get all rooms
    const res = await db.ref('room_details').once('value')
    const list = map(res.val(), (room, key) => ({ room_id: key, ...room }))

    // check exist room
    const existRoom = find(list, (item) => {
      return schoolDetail.id === item.school_id
        && roomDetail.year === item.year
        && roomDetail.level === item.level
        && roomDetail.room === item.room
    })

    return existRoom
  }

  validate = (values) => {
    const errors = {
      teacherDetail: {},
      schoolDetail: {},
      roomDetail: {},
    }
    if (!values.teacherDetail.gender) {
      errors.teacherDetail.gender = 'กรุณากรอกคำนำหน้าชื่อ'
    }
    if (!values.teacherDetail.name) {
      errors.teacherDetail.name = 'กรุณากรอกชื่อ'
    }
    if (!values.teacherDetail.surname) {
      errors.teacherDetail.surname = 'กรุณากรอกนามสกุล'
    }
    if (!values.teacherDetail.nickname) {
      errors.teacherDetail.nickname = 'กรุณากรอกชื่อเล่น'
    }
    if (!values.schoolDetail.name) {
      errors.schoolDetail.name = 'กรุณากรอกชื่อโรงเรียน'
    }
    if (!values.roomDetail.year) {
      errors.roomDetail.year = 'กรุณากรอกปีการศึกษา'
    }
    if (!values.roomDetail.level) {
      errors.roomDetail.level = 'กรุณากรอกระดับชั้น'
    }
    if (!values.roomDetail.room) {
      errors.roomDetail.room = 'กรุณากรอกห้องที่'
    }

    if (isEmpty(errors.teacherDetail) && isEmpty(errors.schoolDetail) && isEmpty(errors.roomDetail)) {
      return {}
    }
    return errors
  }

  onSubmit = async (values, { setFieldValue }) => {
    const { teacherDetail, schoolDetail, roomDetail } = values
    const { isChangeAvatar } = this.state

    this.setState({ isLoading: true })
    let obj = { ...values }
    const existRoom = await this.checkExistRoom(values)

    if (isChangeAvatar) {
      const path = await uploadImage(teacherDetail.avatar, `images/image_teacher/${this.userId}/avatar.jpg`)
      setFieldValue('teacherDetail.avatar', path)
      obj.teacherDetail.avatar = path
    }

    if (isEmpty(existRoom)) {
      const newRoomId = await db.ref('room_details').push({
        school_id: schoolDetail.id,
        year: roomDetail.year,
        level: roomDetail.level,
        room: roomDetail.room,
      }).key
      obj.teacherDetail.room_id = newRoomId
    } else {
      obj.teacherDetail.room_id = existRoom.room_id
    }

    await db.ref(`teacher_details/${this.userId}`).set(obj.teacherDetail)
    this.setState({ isLoading: false })
    this.props.navigation.navigate('Main')
  }

  render() {
    const {
      navigation,
    } = this.props
    const { isLoading,
      isShowSchoolListModal,
      isShowCamera,
      isShowImage,
      schoolList,
      schoolDetail,
      roomDetail,
      teacherDetail,
    } = this.state
    return (
      <View style={Styles.container}>
        <NavigationEvents
          onWillFocus={this.onWillFocus}
        />
        <Formik
          enableReinitialize={true}
          initialValues={{
            schoolDetail,
            roomDetail,
            teacherDetail,
          }}
          validate={this.validate}
          onSubmit={this.onSubmit}
        >
          {(formProps) => {
            const {
              errors,
              handleChange,
              handleBlur,
              handleSubmit,
              setFieldValue,
              touched,
              values,
            } = formProps
            return (
              <View style={{ flex: 1 }}>
                {isLoading && <Loader /> }
                {isShowCamera && <CameraPicker onTakePhoto={photo => this.takePhotoHandler(photo.uri, setFieldValue)} onClose={() => this.setState({ isShowCamera: false })} />}
                {isShowImage && <ImageViewer uri={values.teacherDetail.avatar} onClose={() => this.setState({ isShowImage: false })} />}
                <Topbar onPress={() => navigation.toggleDrawer()} />
                <KeyboardAvoidingView style={{ flex: 1 }} behavior="padding">
                  <ScrollView style={Styles.container}>
                    <View style={styleSelf.profileBox}>
                      <CommonText style={styleSelf.title}>{teacherDetail.gender ? 'แก้ไขข้อมูล' : 'เพิ่มข้อมูล'}</CommonText>
                      <CommonText style={styleSelf.title}>สำหรับการใช้งาน</CommonText>
                      <View style={styleSelf.addAreaWrapper}>
                        <View style={{ width: 250, height: 250 }}>
                          {
                            values.teacherDetail.avatar ? (
                              <TouchableOpacity onPress={() => this.setState({ isShowImage: true })}>
                                <Thumbnail
                                  square
                                  style={{ height: 250, width: 250 }}
                                  source={{ uri: values.teacherDetail.avatar }}
                                />
                              </TouchableOpacity>
                            ) : (
                              <View style={styleSelf.addArea}>
                                <FontAwesome name='plus' color={Colors.white} size={26} />
                              </View>
                            )
                          }
                        </View>
                        <View style={styleSelf.photoBtnGroup}>
                          <Button iconLeft light style={styleSelf.photoBtn} onPress={() => this.setState({ isShowCamera: true })}>
                            <FontAwesome name='camera' />
                            <ButtonText style={{ color: Styles.textColor }}>ถ่ายรูป</ButtonText>
                          </Button>
                          <Button iconLeft light style={styleSelf.photoBtn} onPress={() => this.pickImage(setFieldValue)}>
                            <FontAwesome name='photo' />
                            <ButtonText style={{ color: Styles.textColor }}>เลือกรูป</ButtonText>
                          </Button>
                        </View>
                      </View>
                      <Grid>
                        <Row style={Styles.my10}>
                          <CommonText style={styleSelf.subTitle}>ข้อมูลครู</CommonText>
                        </Row>
                        <Row style={Styles.mb20}>
                          <Col>
                            <CommonText style={styleSelf.label}>คำนำหน้าชื่อ <RequireSign /></CommonText>
                            <Radio
                              radioList={[
                                { label: 'นาย', value: 'male' },
                                { label: 'นาง, นางสาว', value: 'female' }
                              ]}
                              onPress={handleChange('teacherDetail.gender')}
                              value={values.teacherDetail.gender}
                            />
                            { get(errors, 'teacherDetail.gender') && get(touched, 'teacherDetail.gender') && <ValidationText>{errors.teacherDetail.gender}</ValidationText>}
                          </Col>
                        </Row>

                        <Row style={Styles.mb20}>
                          <Col size={50} style={Styles.mr10}>
                            <CommonText style={styleSelf.label}>ชื่อ <RequireSign /></CommonText>
                            <Item regular>
                              <Input
                                onChangeText={handleChange('teacherDetail.name')}
                                onBlur={handleBlur('teacherDetail.name')}
                                value={values.teacherDetail.name}
                              />
                            </Item>
                            { get(errors, 'teacherDetail.name') && get(touched, 'teacherDetail.name') && <ValidationText>{errors.teacherDetail.name}</ValidationText>}
                          </Col>
                          <Col size={50}>
                            <CommonText style={styleSelf.label}>นามสกุล <RequireSign /></CommonText>
                            <Item regular>
                              <Input
                                onChangeText={handleChange('teacherDetail.surname')}
                                onBlur={handleBlur('teacherDetail.surname')}
                                value={values.teacherDetail.surname}
                              />
                            </Item>
                            { get(errors, 'teacherDetail.surname') && get(touched, 'teacherDetail.surname') && <ValidationText>{errors.teacherDetail.surname}</ValidationText>}
                          </Col>
                        </Row>
                        <Row style={Styles.mb20}>
                          <Col>
                            <CommonText style={styleSelf.label}>ชื่อเล่น <RequireSign /></CommonText>
                            <Item regular>
                              <Input
                                onChangeText={handleChange('teacherDetail.nickname')}
                                onBlur={handleBlur('teacherDetail.nickname')}
                                value={values.teacherDetail.nickname}
                              />
                            </Item>
                            { get(errors, 'teacherDetail.nickname') && get(touched, 'teacherDetail.nickname') && <ValidationText>{errors.teacherDetail.nickname}</ValidationText>}
                          </Col>
                        </Row>

                        <Row style={Styles.my10}>
                          <CommonText style={styleSelf.subTitle}>ข้อมูลสถาบันการศึกษา</CommonText>
                        </Row>
                        <Row style={Styles.mb20}>
                          <Col>
                            <CommonText style={styleSelf.label}>ชื่อสถาบันการศึกษา <RequireSign /></CommonText>
                            <Item regular>
                              <Input
                                onBlur={handleBlur('schoolDetail.name')}
                                onFocus={() => this.setState({ isShowSchoolListModal: true })}
                                value={values.schoolDetail.name}
                              />
                            </Item>
                            { get(errors, 'schoolDetail.name') && get(touched, 'schoolDetail.name') && <ValidationText>{errors.schoolDetail.name}</ValidationText>}
                            <ModalFilterPicker
                              visible={isShowSchoolListModal}
                              placeholderText="ค้นหาโรงเรียน"
                              onSelect={picked => this.onSelectSchoolList(picked, setFieldValue)}
                              onCancel={() => this.setState({ isShowSchoolListModal: false })}
                              options={schoolList}
                            />
                          </Col>
                        </Row>
                        <Row style={Styles.mb20}>
                          <Col size={50} style={Styles.mr10}>
                            <CommonText style={styleSelf.label}>จังหวัด</CommonText>
                            <Item regular>
                              <Input
                                editable={false}
                                value={values.schoolDetail.province}
                              />
                            </Item>
                          </Col>
                          <Col size={50}>
                            <CommonText style={styleSelf.label}>อำเภอ</CommonText>
                            <Item regular>
                              <Input
                                editable={false}
                                value={values.schoolDetail.city}
                              />
                            </Item>
                          </Col>
                        </Row>

                        <Row style={Styles.my10}>
                          <CommonText style={styleSelf.subTitle}>ข้อมูลการสอน</CommonText>
                        </Row>
                        <Row style={Styles.mb20}>
                          <Col>
                            <CommonText style={styleSelf.label}>ปีการศึกษา <RequireSign /></CommonText>
                            <Item regular>
                              <Input
                                onChangeText={handleChange('roomDetail.year')}
                                onBlur={handleBlur('roomDetail.year')}
                                value={values.roomDetail.year}
                              />
                            </Item>
                            { get(errors, 'roomDetail.year') && get(touched, 'roomDetail.year') && <ValidationText>{errors.roomDetail.year}</ValidationText>}
                          </Col>
                        </Row>
                        <Row style={Styles.mb20}>
                          <Col size={50} style={Styles.mr10}>
                            <CommonText style={styleSelf.label}>ระดับชั้น <RequireSign /></CommonText>
                            <SelectOption
                              items={[
                                { label: 'เตรียมอนุบาล', value: 'เตรียมอนุบาล' },
                                { label: 'อนุบาล 1', value: 'อนุบาล 1' },
                                { label: 'อนุบาล 2', value: 'อนุบาล 2' },
                                { label: 'อนุบาล 3', value: 'อนุบาล 3' },
                              ]}
                              style={{ alignSelf: 'stretch' }}
                              placeholder={{ label: 'เลือกระดับชั้น' }}
                              value={values.roomDetail.level}
                              onValueChange={value => setFieldValue('roomDetail.level', value)}
                            />
                            { get(errors, 'roomDetail.level') && get(touched, 'roomDetail.level') && <ValidationText>{errors.roomDetail.level}</ValidationText>}
                          </Col>
                          <Col size={50}>
                            <CommonText style={styleSelf.label}>ห้องที่ <RequireSign /></CommonText>
                            <Item regular>
                              <Input
                                onChangeText={handleChange('roomDetail.room')}
                                onBlur={handleBlur('roomDetail.room')}
                                value={values.roomDetail.room}
                              />
                            </Item>
                            { get(errors, 'roomDetail.room') && get(touched, 'roomDetail.room') && <ValidationText>{errors.roomDetail.room}</ValidationText>}
                          </Col>
                        </Row>
                        <Row>
                          <Col>
                            <Button
                              block
                              style={Styles.my10}
                              onPress={handleSubmit}
                            >
                              <ButtonText>บันทึก</ButtonText>
                            </Button>
                          </Col>
                        </Row>
                      </Grid>
                    </View>
                  </ScrollView>
                </KeyboardAvoidingView>
              </View>
            )
          }}
        </Formik>
      </View>
    )
  }
}

export default withNavigationFocus(TeacherProfileScreen)

const styleSelf = StyleSheet.create({
  profileBox: {
    backgroundColor: Colors.greyLight,
    paddingVertical: 50,
    paddingHorizontal: 20,
  },
  title: {
    fontSize: 20,
    color: Colors.titleColor,
    textAlign: 'center',
    fontWeight: 'bold',
  },
  subTitle: {
    fontSize: 18,
    color: Colors.subTitleColor,
    fontWeight: 'bold',
  },
  label: {
    fontWeight: 'bold',
    marginTop: 10,
    fontSize: 16,
  },
  addAreaWrapper: {
    justifyContent: 'center',
    alignItems: 'center',
    marginVertical: 20,
  },
  addArea: {
    width: 250,
    height: 250,
    backgroundColor: Colors.grey,
    justifyContent: 'center',
    alignItems: 'center',
  },
  photoBtnGroup: {
    flexDirection: 'row',
    marginHorizontal: 10
  },
  photoBtn: {
    margin: 5,
    padding: 10,
  },
})
