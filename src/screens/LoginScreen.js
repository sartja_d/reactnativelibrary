import React from 'react'
import {
  ScrollView,
  StyleSheet,
  TouchableOpacity,
  View,
} from 'react-native'
import {
  Button,
  CheckBox,
  Form,
  Input,
  Item,
  Toast,
} from 'native-base'
import firebase from 'firebase/app'
import { Constants, Google, Facebook } from 'expo'
import { get } from 'lodash'
import { ButtonText, CommonText, Line, Loader } from '../components'
import { auth } from '../firebase'
import facebookConfig from '../firebase/facebookConfig'
import googleConfig from '../firebase/googleConfig'
import Styles from '../constants/Styles'
import Colors from '../constants/Colors'

export default class LoginScreen extends React.Component {
  state = {
    email: '',
    password: '',
    isRemember: false,
    isLoading: false,
  }

  userId = get(auth, 'currentUser.uid')

  componentDidMount() {
    if (this.userId) {
      this.props.navigation.navigate('Main')
    }
  }

  loginByEmail = () => {
    const { email, password, isRemember } = this.state
    this.setState({ isLoading: true })
    auth.signInWithEmailAndPassword(email, password)
    .then(() => {
      this.setState({ isLoading: false })
      if (isRemember) {
        auth.setPersistence(firebase.auth.Auth.Persistence.LOCAL)
      }
      this.props.navigation.navigate('Main')
    })
    .catch((error) => {
      this.setState({ isLoading: false })
      Toast.show({
        text: error.message,
        buttonText: 'close',
        type: 'danger',
        position: 'top',
        duration: 3000
      })
    })
  }

  loginByGoogle = async () => {
    const { navigate } = this.props.navigation
    const { type, idToken } = await Google.logInAsync({
      clientId: Constants.appOwnership === 'standalone' ? googleConfig.standaloneClientId : googleConfig.expoClientId,
      scopes: ['profile', 'email'],
    })
    if (type === 'success') {
      await auth.setPersistence(firebase.auth.Auth.Persistence.LOCAL)
      const credential = firebase.auth.GoogleAuthProvider.credential(idToken)
      const profileData = await auth.signInWithCredential(credential)
      const profile = profileData.additionalUserInfo.profile
      const obj = {
        name: profile.given_name,
        surname: profile.family_name,
        email: profile.email,
      }
      if (get(profileData, 'additionalUserInfo.isNewUser')) {
        navigate('TeacherProfile', { profileData: obj })
      } else {
        navigate('Main', { profileData: obj })
      }
    } else {
      Toast.show({
        text: 'Google Login fail',
        buttonText: 'close',
        type: 'danger',
        position: 'top',
        duration: 3000
      })
    }
  }

  loginByFacebook = async () => {
    const { navigate } = this.props.navigation
    const appId = facebookConfig.appId
    const permissions = ['public_profile', 'email']
    const { type, token } = await Facebook.logInWithReadPermissionsAsync(
      appId,
      { permissions }
    )

    if (type === 'success') {
      await auth.setPersistence(firebase.auth.Auth.Persistence.LOCAL)
      const credential = firebase.auth.FacebookAuthProvider.credential(token)
      const profileData = await auth.signInWithCredential(credential)
      if (get(profileData, 'additionalUserInfo.isNewUser')) {
        const profile = profileData.additionalUserInfo.profile
        const obj = {
          name: profile.first_name,
          surname: profile.last_name,
          email: profile.email,
        }
        navigate('TeacherProfile', { profileData: obj })
      } else {
        navigate('Main')
      }
    } else {
      Toast.show({
        text: 'Facebook Login fail',
        buttonText: 'close',
        type: 'danger',
        position: 'top',
        duration: 3000
      })
    }
  }

  render() {
    const { isLoading, isRemember } = this.state
    const { navigate } = this.props.navigation
    return (
      <View style={Styles.container}>
        { isLoading && <Loader /> }
        <ScrollView style={Styles.container} contentContainerStyle={Styles.contentContainer}>
          <View style={styleSelf.loginBox}>
            <CommonText style={styleSelf.title}>ระบบยืน-คืนนิทาน</CommonText>
            <CommonText style={styleSelf.title}>ยินดีต้อนรับ!</CommonText>
            <Form>
              <Item regular style={Styles.my20}>
                <Input
                  placeholder='ชื่อผู้ใช้งาน'
                  placeholderTextColor={Colors.subTitleColor}
                  autoCapitalize='none'
                  onChangeText={ (text) => { this.setState({ email: text }) }}
                />
              </Item>
              <Item regular style={Styles.mb20}>
                <Input
                  placeholder='รหัสผ่าน'
                  placeholderTextColor={Colors.subTitleColor}
                  secureTextEntry
                  onChangeText={ (text) => { this.setState({ password: text }) }}
                />
              </Item>
              <View style={styleSelf.checkbox}>
                <CheckBox checked={isRemember} style={{ marginRight: 20 }} onPress={ () => { this.setState({ isRemember: !isRemember }) }} />
                <CommonText>จดจำรหัสผ่าน</CommonText>
              </View>

              <Button block onPress={this.loginByEmail}>
                <ButtonText>เข้าสู่ระบบ</ButtonText>
              </Button>
              <Line style={Styles.my20} />
              <Button
                iconLeft
                block
                style={{ ...Styles.mb20, backgroundColor: '#ea4335' }}
                onPress={this.loginByGoogle}
              >
                <ButtonText>เข้าสู่ระบบด้วย Google</ButtonText>
              </Button>
              <Button
                iconLeft
                block
                style={{ backgroundColor: '#3b5998' }}
                onPress={this.loginByFacebook}
              >
                <ButtonText>เข้าสู่ระบบด้วย Facebook</ButtonText>
              </Button>
              <Line style={Styles.my20} />
            </Form>
            <TouchableOpacity onPress={() => navigate('forgetPassword')}>
              <CommonText style={styleSelf.subTitle}>ลืมรหัสผ่าน?</CommonText>
            </TouchableOpacity>
            <TouchableOpacity onPress={() => navigate('Register')}>
              <CommonText style={styleSelf.subTitle}>สร้างบัญชีผู้ใช้งานใหม่!</CommonText>
            </TouchableOpacity>
          </View>
        </ScrollView>
      </View>
    )
  }
}

const styleSelf = StyleSheet.create({
  loginBox: {
    backgroundColor: Colors.white,
    margin: 30,
    paddingVertical: 50,
    paddingHorizontal: 30,
    borderRadius: 50,
  },
  title: {
    fontSize: 20,
    color: Colors.titleColor,
    textAlign: 'center'
  },
  subTitle: {
    color: Colors.subTitleColor,
    textAlign: 'center',
    marginBottom: 10,
  },
  checkbox: {
    flex: 1,
    flexDirection: 'row',
    marginBottom: 20,
  }
})
