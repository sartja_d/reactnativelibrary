import React from 'react'
import {
  TouchableOpacity,
  StyleSheet,
  View,
} from 'react-native'
import { FontAwesome } from '@expo/vector-icons'
import { auth } from '../firebase'
import { CommonText } from '../components'
import Colors from '../constants/Colors'

class SidebarScreen extends React.Component {
  logout = async () => {
    await auth.signOut()
    this.props.navigation.navigate('Login')
  }

  render() {
    const { navigate } = this.props.navigation
    return (
      <View style={styleSelf.sidebarList}>
        <TouchableOpacity onPress={() => navigate('TeacherProfile')}>
          <View style={{ ...styleSelf.sidebarItem, borderLeftColor: '#56c887' }}>
            <CommonText style={{ ...styleSelf.sidebarText, color: '#56c887' }}>แก้ไขข้อมูลส่วนตัว</CommonText>
            <FontAwesome name='user' color='#56c887' size={20} />
          </View>
        </TouchableOpacity>
        <TouchableOpacity onPress={() => navigate('Main')}>
          <View style={{ ...styleSelf.sidebarItem, borderLeftColor: '#34b7cb' }}>
            <CommonText style={{ ...styleSelf.sidebarText, color: '#34b7cb' }}>ยืม-คืนนิทาน</CommonText>
            <FontAwesome name='inbox' color='#34b7cb' size={20} />
          </View>
        </TouchableOpacity>
        <TouchableOpacity onPress={() => navigate('Summary')}>
          <View style={{ ...styleSelf.sidebarItem, borderLeftColor: '#fbdd94' }}>
            <CommonText style={{ ...styleSelf.sidebarText, color: '#fbdd94' }}>สรุปผลและส่งออก</CommonText>
            <FontAwesome name='line-chart' color='#fbdd94' size={20} />
          </View>
        </TouchableOpacity>
        <TouchableOpacity onPress={this.logout}>
          <View style={{ ...styleSelf.sidebarItem, borderLeftColor: '#5472d4' }}>
            <CommonText style={{ ...styleSelf.sidebarText, color: '#5472d4' }}>ออกจากระบบ</CommonText>
            <FontAwesome name='power-off' color='#5472d4' size={20} />
          </View>
        </TouchableOpacity>
      </View>
    )
  }
}

export default SidebarScreen

const styleSelf = StyleSheet.create({
  sidebarList: {
    flex: 1,
    backgroundColor: Colors.greyLight,
    paddingVertical: 60,
  },
  sidebarItem: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    backgroundColor: Colors.white,
    margin: 10,
    paddingHorizontal: 20,
    paddingVertical: 10,
    borderBottomLeftRadius: 5,
    borderBottomRightRadius: 5,
    borderTopLeftRadius: 5,
    borderTopRightRadius: 5,
    borderBottomWidth: 5,
    borderRightWidth: 5,
    borderTopWidth: 5,
    borderLeftWidth: 5,
    borderBottomColor: Colors.white,
    borderRightColor: Colors.white,
    borderTopColor: Colors.white,
  },
  sidebarText: {
    fontSize: 18,
  },
})
