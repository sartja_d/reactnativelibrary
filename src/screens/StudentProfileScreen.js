import React from 'react'
import {
  KeyboardAvoidingView,
  ScrollView,
  StyleSheet,
  TouchableOpacity,
  View,
} from 'react-native'
import {
  Button,
  Input,
  Item,
  Thumbnail,
} from 'native-base'
import { ImagePicker } from 'expo'
import { FontAwesome } from '@expo/vector-icons'
import { withNavigationFocus, NavigationEvents } from 'react-navigation'
import { Col, Row, Grid } from 'react-native-easy-grid'
import DatePicker from 'react-native-datepicker'
import { Formik } from 'formik'
import { get } from 'lodash'
import {
  ButtonText,
  CameraPicker,
  CommonText,
  ImageViewer,
  Loader,
  Radio,
  RequireSign,
  Topbar,
  ValidationText,
} from '../components'
import { auth, db } from '../firebase'
import { uploadImage } from '../utils'
import Styles from '../constants/Styles'
import Colors from '../constants/Colors'

class StudentProfileScreen extends React.Component {
  state = {
    isChangeAvatar: false,
    isShowCamera: false,
    isShowImage: false,
    isLoading: false,
    studentDetail: {
      avatar: '',
      gender: '',
      name: '',
      surname: '',
      nickname: '',
      birthDate: '',
      father: '',
      mother: '',
      parent: '',
      tel: '',
      room_id: '',
    }
  }

  userId = get(auth, 'currentUser.uid')
  studentId = ''

  onWillFocus = async () => {
    if (!this.userId) {
      this.props.navigation.navigate('Login')
    } else {
      this.setState({ isLoading: true })
      this.studentId = this.props.navigation.getParam('studentId')
      const resTeacher = await db.ref(`teacher_details/${this.userId}`).once('value')
      const resStudent = await db.ref(`student_details/${this.studentId}`).once('value')
      this.setState({
        studentDetail: {
          ...this.state.studentDetail,
          ...resStudent.val(),
          room_id: get(resTeacher.val(), 'room_id')
        },
        isLoading: false,
      })
    }
  }

  takePhotoHandler = async (photo, setFieldValue) => {
    setFieldValue('avatar', photo)
    this.setState({ isChangeAvatar: true, isShowCamera: false })
  }

  pickImage = async (setFieldValue) => {
    let result = await ImagePicker.launchImageLibraryAsync({
      mediaTypes: 'Images',
      allowsEditing: true,
      aspect: [4, 3],
    })
    if (!result.cancelled) {
      setFieldValue('avatar', result.uri)
      this.setState({ isChangeAvatar: true })
    }
  }

  validate = (values) => {
    const errors = {}
    if (!values.gender) {
      errors.gender = 'กรุณากรอกคำนำหน้าชื่อ'
    }
    if (!values.name) {
      errors.name = 'กรุณากรอกชื่อ'
    }
    if (!values.surname) {
      errors.surname = 'กรุณากรอกนามสกุล'
    }
    if (!values.nickname) {
      errors.nickname = 'กรุณากรอกชื่อเล่น'
    }
    if (!values.birthDate) {
      errors.birthDate = 'กรุณากรอกวันเกิด'
    }
    return errors
  }

  onSubmit = async (values, { setFieldValue }) => {
    const { isChangeAvatar } = this.state
    this.setState({ isLoading: true })
    let obj = {
      ...values,
    }

    if (this.studentId) {
      if (isChangeAvatar) {
        const imgPath = await uploadImage(values.avatar, `images/image_student/${this.studentId}/avatar.jpg`)
        setFieldValue('avatar', imgPath)
        obj.avatar = imgPath
      }
      await db.ref(`student_details/${this.studentId}`).set(obj)
      this.setState({ isLoading: false })
      this.props.navigation.navigate('Main')
    } else {
      const studentId = await db.ref('student_details').push(obj).key
      if (isChangeAvatar) {
        const imgPath = await uploadImage(values.avatar, `images/image_student/${studentId}/avatar.jpg`)
        setFieldValue('avatar', imgPath)
        await db.ref(`student_details/${studentId}`).update({ avatar: imgPath })
      }
      this.setState({ isLoading: false })
      this.props.navigation.navigate('Main')
    }
  }

  render() {
    const {
      navigation,
    } = this.props
    const { isLoading, isShowCamera, isShowImage, studentDetail } = this.state
    return (
      <View style={Styles.container}>
        <NavigationEvents
          onWillFocus={this.onWillFocus}
        />
        <Formik
          enableReinitialize={true}
          initialValues={studentDetail}
          validate={this.validate}
          onSubmit={this.onSubmit}
        >
          {(formProps) => {
            const {
              errors,
              handleChange,
              handleBlur,
              handleSubmit,
              setFieldValue,
              touched,
              values,
            } = formProps
            return (
              <View style={{ flex: 1 }}>
                {isLoading && <Loader /> }
                {isShowCamera && <CameraPicker onTakePhoto={photo => this.takePhotoHandler(photo.uri, setFieldValue)} onClose={() => this.setState({ isShowCamera: false })} />}
                {isShowImage && <ImageViewer uri={values.avatar} onClose={() => this.setState({ isShowImage: false })} />}
                <Topbar onPress={() => navigation.toggleDrawer()} />
                <KeyboardAvoidingView style={{ flex: 1 }} behavior="padding">
                  <ScrollView style={Styles.container}>
                    <View style={styleSelf.profileBox}>
                      <CommonText style={styleSelf.title}>{studentDetail.gender ? 'แก้ไขข้อมูลนักเรียน' : 'เพิ่มข้อมูลนักเรียน'}</CommonText>
                      <CommonText style={styleSelf.title}>สำหรับการใช้งาน</CommonText>
                      <View style={styleSelf.addAreaWrapper}>
                        <View style={{ width: 250, height: 250 }}>
                          {
                            values.avatar ? (
                              <TouchableOpacity onPress={() => this.setState({ isShowImage: true })}>
                                <Thumbnail
                                  square
                                  style={{ height: 250, width: 250 }}
                                  source={{ uri: values.avatar }}
                                />
                              </TouchableOpacity>
                            ) : (
                              <View style={styleSelf.addArea}>
                                <FontAwesome name='plus' color={Colors.white} size={26} />
                              </View>
                            )
                          }
                        </View>
                        <View style={styleSelf.photoBtnGroup}>
                          <Button iconLeft light style={styleSelf.photoBtn} onPress={() => this.setState({ isShowCamera: true })}>
                            <FontAwesome name='camera' />
                            <ButtonText style={{ color: Styles.textColor }}>ถ่ายรูป</ButtonText>
                          </Button>
                          <Button iconLeft light style={styleSelf.photoBtn} onPress={() => this.pickImage(setFieldValue)}>
                            <FontAwesome name='photo' />
                            <ButtonText style={{ color: Styles.textColor }}>เลือกรูป</ButtonText>
                          </Button>
                        </View>
                      </View>
                      <Grid>
                        <Row style={Styles.my10}>
                          <CommonText style={styleSelf.subTitle}>ข้อมูลนักเรียน</CommonText>
                        </Row>
                        <Row style={Styles.mb20}>
                          <Col>
                            <CommonText style={styleSelf.label}>คำนำหน้าชื่อ <RequireSign /></CommonText>
                            <Radio
                              radioList={[
                                { label: 'ด.ช.', value: 'male' },
                                { label: 'ด.ญ.', value: 'female' }
                              ]}
                              onPress={handleChange('gender')}
                              value={values.gender}
                            />
                            { errors.gender && touched.gender && <ValidationText>{errors.gender}</ValidationText>}
                          </Col>
                        </Row>

                        <Row style={Styles.mb20}>
                          <Col size={50} style={Styles.mr10}>
                            <CommonText style={styleSelf.label}>ชื่อ <RequireSign /></CommonText>
                            <Item regular>
                              <Input
                                onChangeText={handleChange('name')}
                                onBlur={handleBlur('name')}
                                value={values.name}
                              />
                            </Item>
                            { errors.name && touched.name && <ValidationText>{errors.name}</ValidationText>}
                          </Col>
                          <Col size={50}>
                            <CommonText style={styleSelf.label}>นามสกุล <RequireSign /></CommonText>
                            <Item regular>
                              <Input
                                onChangeText={handleChange('surname')}
                                onBlur={handleBlur('surname')}
                                value={values.surname}
                              />
                            </Item>
                            { errors.surname && touched.surname && <ValidationText>{errors.surname}</ValidationText>}
                          </Col>
                        </Row>
                        <Row style={Styles.mb20}>
                          <Col>
                            <CommonText style={styleSelf.label}>ชื่อเล่น <RequireSign /></CommonText>
                            <Item regular>
                              <Input
                                onChangeText={handleChange('nickname')}
                                onBlur={handleBlur('nickname')}
                                value={values.nickname}
                              />
                            </Item>
                            { errors.nickname && touched.nickname && <ValidationText>{errors.nickname}</ValidationText>}
                          </Col>
                        </Row>
                        <Row style={Styles.mb20}>
                          <Col>
                            <CommonText style={styleSelf.label}>วันเกิด (วัน/เดือน/ปี) <RequireSign /></CommonText>
                            <Item regular>
                              <DatePicker
                                style={{ width: '100%' }}
                                customStyles={{
                                  dateInput: { padding: 5, borderWidth: 0, alignItems: 'flex-start' },
                                  dateText: { color: values.birthDate ? 'black' : 'grey' }
                                }}
                                date={values.birthDate}
                                mode="date"
                                format="DD/MM/YYYY"
                                confirmBtnText="Confirm"
                                cancelBtnText="Cancel"
                                onDateChange={handleChange('birthDate')}
                              />
                            </Item>
                            { errors.birthDate && touched.birthDate && <ValidationText>{errors.birthDate}</ValidationText>}
                          </Col>
                        </Row>

                        <Row style={Styles.my10}>
                          <CommonText style={styleSelf.subTitle}>ข้อมูลผู้ปกครอง</CommonText>
                        </Row>
                        <Row style={Styles.mb20}>
                          <Col>
                            <CommonText style={styleSelf.label}>ชื่อ-นามสกุล บิดา</CommonText>
                            <Item regular>
                              <Input
                                onChangeText={handleChange('father')}
                                onBlur={handleBlur('father')}
                                value={values.father}
                              />
                            </Item>
                          </Col>
                        </Row>
                        <Row style={Styles.mb20}>
                          <Col>
                            <CommonText style={styleSelf.label}>ชื่อ-นามสกุล มารดา</CommonText>
                            <Item regular>
                              <Input
                                onChangeText={handleChange('mother')}
                                onBlur={handleBlur('mother')}
                                value={values.mother}
                              />
                            </Item>
                          </Col>
                        </Row>
                        <Row style={Styles.mb20}>
                          <Col>
                            <CommonText style={styleSelf.label}>ชื่อ-นามสกุล ผู้ปกครอง</CommonText>
                            <Item regular>
                              <Input
                                onChangeText={handleChange('parent')}
                                onBlur={handleBlur('parent')}
                                value={values.parent}
                              />
                            </Item>
                          </Col>
                        </Row>
                        <Row style={Styles.mb20}>
                          <Col>
                            <CommonText style={styleSelf.label}>เบอร์ติดต่อของ บิดา มารดา หรือผู้ปกครอง</CommonText>
                            <Item regular>
                              <Input
                                onChangeText={handleChange('tel')}
                                onBlur={handleBlur('tel')}
                                value={values.tel}
                              />
                            </Item>
                          </Col>
                        </Row>
                        <Row>
                          <Col>
                            <Button
                              block
                              style={Styles.my10}
                              onPress={handleSubmit}
                            >
                              <ButtonText>บันทึก</ButtonText>
                            </Button>
                          </Col>
                        </Row>
                      </Grid>
                    </View>
                  </ScrollView>
                </KeyboardAvoidingView>
              </View>
            )
          }}
        </Formik>
      </View>
    )
  }
}

export default withNavigationFocus(StudentProfileScreen)

const styleSelf = StyleSheet.create({
  profileBox: {
    backgroundColor: Colors.greyLight,
    paddingVertical: 50,
    paddingHorizontal: 20,
  },
  title: {
    fontSize: 20,
    color: Colors.titleColor,
    textAlign: 'center',
    fontWeight: 'bold',
  },
  subTitle: {
    fontSize: 18,
    color: Colors.subTitleColor,
    fontWeight: 'bold',
  },
  label: {
    fontWeight: 'bold',
    marginTop: 10,
    fontSize: 16,
  },
  addAreaWrapper: {
    justifyContent: 'center',
    alignItems: 'center',
    marginVertical: 20,
  },
  addArea: {
    width: 250,
    height: 250,
    backgroundColor: Colors.grey,
    justifyContent: 'center',
    alignItems: 'center',
  },
  photoBtnGroup: {
    flexDirection: 'row',
    marginHorizontal: 10
  },
  photoBtn: {
    margin: 5,
    padding: 10,
  },
})
