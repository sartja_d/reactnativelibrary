import { storage } from '../firebase'

const imageUri = (uri) => {
  return uri ? { uri } : require('../../assets/images/default-avatar.jpg')
}

const removeImage = (directory) => {
  storage.ref().child(directory).delete()
}

const uploadImage = async (uri, directory) => {
  const blob = await new Promise((resolve, reject) => {
    const xhr = new XMLHttpRequest()
    xhr.onload = () => {
      resolve(xhr.response);
    }
    xhr.onerror = () => {
      reject(new TypeError('Network request failed'));
    }
    xhr.responseType = 'blob'
    xhr.open('GET', uri, true)
    xhr.send(null)
  })
  const ref = storage.ref().child(directory)
  const snapshot = await ref.put(blob)
  blob.close()
  const path = await snapshot.ref.getDownloadURL()
  return path
}

export {
  imageUri,
  removeImage,
  uploadImage,
}
