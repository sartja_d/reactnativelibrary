import {
  createAppContainer,
  createStackNavigator,
  createSwitchNavigator,
  createDrawerNavigator,
} from 'react-navigation'
import {
  BorrowScreen,
  forgetPasswordScreen,
  LoginScreen,
  MainScreen,
  RegisterScreen,
  SidebarScreen,
  StudentProfileScreen,
  SummaryScreen,
  TeacherProfileScreen,
} from '../screens'

const AuthNavigation = createStackNavigator(
  {
    Login: LoginScreen,
    Register: RegisterScreen,
    forgetPassword: forgetPasswordScreen,
  },
  {
    initialRouteName: 'Login',
  }
)

const AppNavigation = createStackNavigator(
  {
    Borrow: BorrowScreen,
    Main: MainScreen,
    StudentProfile: StudentProfileScreen,
    Summary:SummaryScreen,
    TeacherProfile: TeacherProfileScreen,
  },
  {
    initialRouteName: 'Main',
  }
)

const DrawerNavigation = createDrawerNavigator({
  Root: AppNavigation,
}, {
  contentComponent: SidebarScreen,
})

const AllNavigation = createSwitchNavigator(
  {
    Auth: AuthNavigation,
    App: DrawerNavigation,
  },
  {
    initialRouteName: 'App',
  }
)

export default createAppContainer(AllNavigation)
